/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.data.response;

import java.util.List;
import java.util.ArrayList;

public class RowResponse {
	private List<SimpleValue> metricValues;
	private List<SimpleValue> dimensionValues;
	
	public RowResponse() {
		metricValues = new ArrayList<>();
		dimensionValues = new ArrayList<>();
	}
	
	
	public List<SimpleValue> getMetricValues() {
		return metricValues;
	}
	public void setMetricValues(List<SimpleValue> metrics) {
		this.metricValues = metrics;
	}
	public List<SimpleValue> getDimensionValues() {
		return dimensionValues;
	}
	public void setDimensionValues(List<SimpleValue> dimensions) {
		this.dimensionValues = dimensions;
	}
	
}
