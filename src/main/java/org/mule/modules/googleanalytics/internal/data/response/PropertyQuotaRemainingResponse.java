/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.data.response;

public class PropertyQuotaRemainingResponse {

	private long consumed;
	private long remaining;
	
	
	public long getConsumed() {
		return consumed;
	}
	public void setConsumed(long consumed) {
		this.consumed = consumed;
	}
	public long getRemaining() {
		return remaining;
	}
	public void setRemaining(long remaining) {
		this.remaining = remaining;
	}
	
}
