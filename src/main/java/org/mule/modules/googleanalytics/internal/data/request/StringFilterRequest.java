/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.data.request;

public class StringFilterRequest {
	private String matchType;
	private String value;
	
	
	public String getMatchType() {
		return matchType;
	}

	public String getValue() {
		return value;
	}

}
