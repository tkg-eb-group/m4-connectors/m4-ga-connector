/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.error;

import static java.util.Optional.ofNullable;
import static org.mule.runtime.extension.api.error.MuleErrors.CONNECTIVITY;

import java.util.Optional;

import org.mule.runtime.extension.api.error.ErrorTypeDefinition;

public enum GoogleAnalyticsErrorType implements ErrorTypeDefinition<GoogleAnalyticsErrorType> {

	BAD_REQUEST,
	INTERNAL_ERROR,
    CONNECTION_EXCEPTION(CONNECTIVITY);

    private ErrorTypeDefinition<?> parent;

    GoogleAnalyticsErrorType(final ErrorTypeDefinition<?> parent) {
        this.parent = parent;
    }

    GoogleAnalyticsErrorType() {

    }

    @Override
    public Optional<ErrorTypeDefinition<? extends Enum<?>>> getParent() {
        return ofNullable(parent);
    }
	
	
	 
	
}
