/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.connection.provider;

import static org.mule.runtime.api.meta.ExpressionSupport.SUPPORTED;

import java.util.concurrent.TimeUnit;

import org.mule.modules.googleanalytics.internal.connection.GoogleAnalyticsConnection;
import org.mule.modules.googleanalytics.internal.error.GoogleAnalyticsErrorType;
import org.mule.modules.googleanalytics.internal.error.exception.GoogleAnalyticsException;
import org.mule.runtime.api.connection.CachedConnectionProvider;
import org.mule.runtime.api.connection.ConnectionProvider;
import org.mule.runtime.api.connection.ConnectionValidationResult;
import org.mule.runtime.api.connection.PoolingConnectionProvider;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.api.meta.model.display.PathModel.Type;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Path;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.gax.rpc.ApiException;


/**
 * This class (as it's name implies) provides connection instances and the functionality to disconnect and validate those
 * connections.
 * <p>
 * All connection related parameters (values required in order to create a connection) must be
 * declared in the connection providers.
 * <p>
 * This particular example is a {@link PoolingConnectionProvider} which declares that connections resolved by this provider
 * will be pooled and reused. There are other implementations like {@link CachedConnectionProvider} which lazily creates and
 * caches connections or simply {@link ConnectionProvider} if you want a new connection each time something requires one.
 */
public class GoogleAnalyticsConnectionProvider implements CachedConnectionProvider<GoogleAnalyticsConnection>  {

	private static final Logger log = LoggerFactory.getLogger(GoogleAnalyticsConnectionProvider.class);

	@Parameter
	@Path(type = Type.FILE, acceptedFileExtensions = "json", acceptsUrls = false)
	@Expression(SUPPORTED)
	@Placement(order = 1)
	private String jsonFilePath;
	
	@Parameter
	@Optional(defaultValue = "10")
	@DisplayName("Timeout")
	@Placement(tab = Placement.ADVANCED_TAB, order = 1)
	@Expression(ExpressionSupport.SUPPORTED)
	private int timeout;

	@Parameter
	@Optional(defaultValue = "SECONDS")
	@DisplayName("Timeout unit")
	@Placement(tab = Placement.ADVANCED_TAB, order = 2)
	@Expression(ExpressionSupport.SUPPORTED)
	private TimeUnit timeoutUnit;

	@Override
	public GoogleAnalyticsConnection connect() {
		GoogleAnalyticsConnection basicConnection = new GoogleAnalyticsConnection(jsonFilePath, timeout, timeoutUnit);

		try {
			basicConnection.connect();
		} catch (Exception ex) {
			log.error("Error occured in GoogleAnalyticsConnectionProvider::connect()", ex);
			throw new GoogleAnalyticsException(ex.getMessage(), GoogleAnalyticsErrorType.CONNECTION_EXCEPTION);
		}

		return basicConnection;
	}

	@Override
	public void disconnect(GoogleAnalyticsConnection connection) {
		try {
			connection.disconnect();
		} catch (Exception e) {
			log.error("Error occured in GoogleAnalyticsConnectionProvider::disconnect()", e);
			throw new GoogleAnalyticsException(e.getMessage(), GoogleAnalyticsErrorType.CONNECTION_EXCEPTION);
		}
	}

	@Override
	public ConnectionValidationResult validate(GoogleAnalyticsConnection connection) {
		try {
			connection.getAnalytics().getMetadata("properties/0/metadata");
			return ConnectionValidationResult.success();
		} catch (ApiException ex) {
			log.error("Unable to get Google Analytics Connection::validate()", ex);
			return ConnectionValidationResult.failure("Connection is not valid", new GoogleAnalyticsException(ex.getMessage(), GoogleAnalyticsErrorType.CONNECTION_EXCEPTION));
		}
	}
}
