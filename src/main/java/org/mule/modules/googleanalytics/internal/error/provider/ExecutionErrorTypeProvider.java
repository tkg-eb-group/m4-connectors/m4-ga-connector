/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.error.provider;

import org.mule.runtime.extension.api.error.ErrorTypeDefinition;


import static org.mule.modules.googleanalytics.internal.error.GoogleAnalyticsErrorType.INTERNAL_ERROR;
import static org.mule.modules.googleanalytics.internal.error.GoogleAnalyticsErrorType.BAD_REQUEST;

import java.util.Set;

public class ExecutionErrorTypeProvider extends BaseErrorTypeProvider {

	@Override
	public void addErrors(Set<ErrorTypeDefinition<?>> errors) {
		errors.add(INTERNAL_ERROR);
		errors.add(BAD_REQUEST);
	}
}