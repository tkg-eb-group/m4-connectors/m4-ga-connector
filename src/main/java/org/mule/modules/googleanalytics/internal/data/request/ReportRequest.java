/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.data.request;

import java.util.List;

public class ReportRequest {
	private List<SimpleItemRequest> dimensions;
	private List<SimpleItemRequest> metrics;
	private List<DateRangeRequest> dateRanges;
	private FilterExpressionRequest dimensionFilter;
	private FilterExpressionRequest metricFilter;
	
	
	public List<SimpleItemRequest> getDimensions() {
		return dimensions;
	}
	
	public List<SimpleItemRequest> getMetrics() {
		return metrics;
	}
	
	public List<DateRangeRequest> getDateRanges() {
		return dateRanges;
	}
	
	public FilterExpressionRequest getDimensionFilter() {
		return dimensionFilter;
	}

	public FilterExpressionRequest getMetricFilter() {
		return metricFilter;
	}
	
}
