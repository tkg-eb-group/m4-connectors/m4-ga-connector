/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.connection;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import org.mule.modules.googleanalytics.internal.error.GoogleAnalyticsErrorType;
import org.mule.modules.googleanalytics.internal.error.exception.GoogleAnalyticsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.threeten.bp.Duration;

import com.google.analytics.data.v1beta.BetaAnalyticsDataClient;
import com.google.analytics.data.v1beta.BetaAnalyticsDataSettings;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.api.gax.retrying.RetrySettings;
import com.google.auth.oauth2.GoogleCredentials;


/**
 * This class contains the necessary code to establish a connection with the GA Analytics Data API
 */
public final class GoogleAnalyticsConnection {
	private static final Logger log = LoggerFactory.getLogger(GoogleAnalyticsConnection.class);
			
	private BetaAnalyticsDataClient analyticsData;
	private final String jsonFilePath;
	private int timeout;
	private TimeUnit timeoutUnit;


	public GoogleAnalyticsConnection(String jsonFilePath, int timeout, TimeUnit timeoutUnit) {
		  this.jsonFilePath = jsonFilePath;
		  this.timeout = timeout;
		  this.timeoutUnit = timeoutUnit;
	}

	// method which provide google Analytics connection
	public void connect() throws IOException {
		InputStream path = GoogleAnalyticsConnection.class.getClassLoader().getResourceAsStream(jsonFilePath);
		GoogleCredentials credentials = null;
		try {
			if(path == null) {
				InputStream filePath = new FileInputStream(jsonFilePath);
				credentials = GoogleCredentials.fromStream(filePath);
			} else
				credentials = GoogleCredentials.fromStream(path);
		} catch (NullPointerException | FileNotFoundException ex) {
			throw new GoogleAnalyticsException("Missing file, please check the path.",
					GoogleAnalyticsErrorType.CONNECTION_EXCEPTION);
		}
		
		Duration duration;
		switch(timeoutUnit) {
			case DAYS: duration = Duration.ofDays(timeout);
			break;
			case HOURS: duration = Duration.ofHours(timeout);
			break;
			case MINUTES: duration = Duration.ofMinutes(timeout);
			break;
			case MILLISECONDS: duration = Duration.ofMillis(timeout);
			break;
			case MICROSECONDS: duration = Duration.ofNanos(timeout * 1000L);
			break;
			case NANOSECONDS: duration = Duration.ofNanos(timeout);
			break;
			default: duration = Duration.ofSeconds(timeout);
		}
		if(log.isDebugEnabled())
			log.debug(duration.toString());
		
		BetaAnalyticsDataSettings.Builder betaAnalyticsDataSettingsBuilder = BetaAnalyticsDataSettings.newBuilder();
		betaAnalyticsDataSettingsBuilder
				.setCredentialsProvider(FixedCredentialsProvider.create(credentials))
				.runReportSettings()
				.setRetrySettings(RetrySettings.newBuilder().setTotalTimeout(duration).build());
		
		analyticsData = BetaAnalyticsDataClient.create(betaAnalyticsDataSettingsBuilder.build());
	}

	public void disconnect() {
		analyticsData.close();
	}

	public BetaAnalyticsDataClient getAnalytics() {
		return analyticsData;
	}
}
