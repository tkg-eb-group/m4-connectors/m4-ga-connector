/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.mule.modules.googleanalytics.api.params.BetweenFilterParameter;
import org.mule.modules.googleanalytics.api.params.CohortParameter;
import org.mule.modules.googleanalytics.api.params.CohortRangeParameter;
import org.mule.modules.googleanalytics.api.params.CohortSpecParameter;
import org.mule.modules.googleanalytics.api.params.DateRangeParameter;
import org.mule.modules.googleanalytics.api.params.DimensionFilterParameter;
import org.mule.modules.googleanalytics.api.params.DimensionParameter;
import org.mule.modules.googleanalytics.api.params.FilterExpressionParameter;
import org.mule.modules.googleanalytics.api.params.FilterParameter;
import org.mule.modules.googleanalytics.api.params.MetricFilterParameter;
import org.mule.modules.googleanalytics.api.params.MetricParameter;
import org.mule.modules.googleanalytics.api.params.NumericFilterParameter;
import org.mule.modules.googleanalytics.api.params.OptionalSettingsParameter;
import org.mule.modules.googleanalytics.api.params.ReportRequestParameter;
import org.mule.modules.googleanalytics.api.params.RunReportRequestParameter;
import org.mule.modules.googleanalytics.api.params.SpecificFilterParameter;
import org.mule.modules.googleanalytics.api.params.StringFilterParameter;
import org.mule.modules.googleanalytics.internal.connection.GoogleAnalyticsConnection;
import org.mule.modules.googleanalytics.internal.data.request.BetweenFilterRequest;
import org.mule.modules.googleanalytics.internal.data.request.DateRangeRequest;
import org.mule.modules.googleanalytics.internal.data.request.FilterExpressionRequest;
import org.mule.modules.googleanalytics.internal.data.request.FilterRequest;
import org.mule.modules.googleanalytics.internal.data.request.NumericFilterRequest;
import org.mule.modules.googleanalytics.internal.data.request.ReportRequest;
import org.mule.modules.googleanalytics.internal.data.request.SimpleItemRequest;
import org.mule.modules.googleanalytics.internal.data.request.StringFilterRequest;
import org.mule.modules.googleanalytics.internal.data.response.MetricHeaderResponse;
import org.mule.modules.googleanalytics.internal.data.response.MetricType;
import org.mule.modules.googleanalytics.internal.data.response.PropertyQuotaResponse;
import org.mule.modules.googleanalytics.internal.data.response.ReportResponse;
import org.mule.modules.googleanalytics.internal.data.response.RowResponse;
import org.mule.modules.googleanalytics.internal.data.response.SimpleObject;
import org.mule.modules.googleanalytics.internal.data.response.SimpleValue;
import org.mule.modules.googleanalytics.internal.error.GoogleAnalyticsErrorType;
import org.mule.modules.googleanalytics.internal.error.exception.GoogleAnalyticsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.analytics.data.v1beta.Cohort;
import com.google.analytics.data.v1beta.CohortSpec;
import com.google.analytics.data.v1beta.CohortsRange;
import com.google.analytics.data.v1beta.CohortsRange.Granularity;
import com.google.analytics.data.v1beta.DateRange;
import com.google.analytics.data.v1beta.Dimension;
import com.google.analytics.data.v1beta.DimensionHeader;
import com.google.analytics.data.v1beta.DimensionValue;
import com.google.analytics.data.v1beta.Filter;
import com.google.analytics.data.v1beta.Filter.BetweenFilter;
import com.google.analytics.data.v1beta.Filter.InListFilter;
import com.google.analytics.data.v1beta.Filter.NumericFilter;
import com.google.analytics.data.v1beta.Filter.NumericFilter.Operation;
import com.google.analytics.data.v1beta.Filter.StringFilter;
import com.google.analytics.data.v1beta.Filter.StringFilter.MatchType;
import com.google.analytics.data.v1beta.FilterExpression;
import com.google.analytics.data.v1beta.FilterExpressionList;
import com.google.analytics.data.v1beta.Metric;
import com.google.analytics.data.v1beta.MetricHeader;
import com.google.analytics.data.v1beta.MetricValue;
import com.google.analytics.data.v1beta.NumericValue;
import com.google.analytics.data.v1beta.PropertyQuota;
import com.google.analytics.data.v1beta.Row;
import com.google.analytics.data.v1beta.RunReportRequest;
import com.google.analytics.data.v1beta.RunReportRequest.Builder;
import com.google.analytics.data.v1beta.RunReportResponse;
import com.google.api.gax.rpc.ApiException;
import com.google.api.gax.rpc.StatusCode.Code;
import com.google.gson.Gson;


public class GoogleAnalyticsUtility {
	private static final Logger log = LoggerFactory.getLogger(GoogleAnalyticsUtility.class);
	
	public String runReport(GoogleAnalyticsConnection connection, String propertyId, ReportRequestParameter reportRequest, OptionalSettingsParameter optionalSettings, CohortSpecParameter cohortSpec) {
		RunReportRequest runReportRequest = null; 
		RunReportResponse response = null;
		
		try {
			runReportRequest = buildRunReportRequest(propertyId, reportRequest, optionalSettings, cohortSpec);
			if(log.isDebugEnabled())
				log.debug(runReportRequest.toString());

			response = connection.getAnalytics().runReport(runReportRequest);
		} catch (ApiException ex) {
			if(ex.getStatusCode().getCode() == Code.UNAUTHENTICATED || ex.getStatusCode().getCode() == Code.PERMISSION_DENIED)
				throw new GoogleAnalyticsException(ex.getMessage(), GoogleAnalyticsErrorType.CONNECTION_EXCEPTION);
			if(ex.getStatusCode().getCode() == Code.INVALID_ARGUMENT) {
				throw new GoogleAnalyticsException(ex.getMessage(), GoogleAnalyticsErrorType.BAD_REQUEST);
			}
		} catch (Exception ex) {			
			throw new GoogleAnalyticsException(ex.getMessage(), GoogleAnalyticsErrorType.INTERNAL_ERROR);
		}
		
		Gson gson = new Gson();
		
		return gson.toJson(generateResponse(response));
	}
	
	private RunReportRequest buildRunReportRequest(String propertyId, ReportRequestParameter reportRequest, OptionalSettingsParameter optionalSettings, CohortSpecParameter cohortSpec) {
		
		Builder runReportRequestBuilder = RunReportRequest.newBuilder();

		runReportRequestBuilder = buildCommonRequest(runReportRequestBuilder, propertyId, optionalSettings, cohortSpec);
		
		RunReportRequestParameter basic = reportRequest.getBasic();
		
		if(basic != null) {
			if(log.isDebugEnabled())
				log.debug("Entering to basic mode");
			runReportRequestBuilder = buildBasicRequest(runReportRequestBuilder, basic.getDateRanges(), basic.getDimensions(), basic.getMetrics(), basic.getDimensionFilter(), basic.getMetricFilter());
		} else {
			if(log.isDebugEnabled())
				log.debug("Entering to advanced mode");
			runReportRequestBuilder = buildAdvancedRequest(runReportRequestBuilder, reportRequest.getAdvanced());
		}
		
		return runReportRequestBuilder.build();
		
	}
	
	private Builder buildBasicRequest(Builder runReportRequestBuilder,List<DateRangeParameter> dateRanges,
			List<DimensionParameter> dimensions, List<MetricParameter> metrics, DimensionFilterParameter dimensionFilter, MetricFilterParameter metricFilter) {
		List<DateRange> gaDateRanges = new ArrayList<>();
		List<Dimension> gaDimensions = new ArrayList<>();
		List<Metric> gaMetrics = new ArrayList<>();
		
		for (DateRangeParameter dateRange : dateRanges) {
			gaDateRanges.add(buildDateRange(dateRange));
		}

		for (DimensionParameter dimension : dimensions) {
			gaDimensions.add(Dimension.newBuilder().setName(dimension.getDimensionValue()).build());
		}
		
		for (MetricParameter metric : metrics) {
			gaMetrics.add(Metric.newBuilder().setName(metric.getMetricValue()).build());
		}
		
		runReportRequestBuilder = runReportRequestBuilder.addAllDimensions(gaDimensions).addAllMetrics(gaMetrics).addAllDateRanges(gaDateRanges);
		
		if(dimensionFilter != null) {
			runReportRequestBuilder.setDimensionFilter(buildExpressionFilterRequest(dimensionFilter.getDimensionFilter()));
		}
		
		if(metricFilter != null) {
			runReportRequestBuilder.setMetricFilter(buildExpressionFilterRequest(metricFilter.getMetricFilter()));
		}
		
		
		return runReportRequestBuilder;
	}
	
	private Builder buildCommonRequest(Builder runReportRequestBuilder, String propertyId, OptionalSettingsParameter optionalSettings, CohortSpecParameter cohortSpec) {
		runReportRequestBuilder.setProperty("properties/" + propertyId);
		runReportRequestBuilder = optionalSettings.getLimit() > 0 ? runReportRequestBuilder.setLimit(optionalSettings.getLimit()) : runReportRequestBuilder;
		runReportRequestBuilder = optionalSettings.getOffset() > 0 ? runReportRequestBuilder.setOffset(optionalSettings.getOffset()) : runReportRequestBuilder;
		runReportRequestBuilder = optionalSettings.getCurrencyCode() != null ? runReportRequestBuilder.setCurrencyCode(optionalSettings.getCurrencyCode()) : runReportRequestBuilder;
		runReportRequestBuilder = optionalSettings.isKeepEmptyRows() ? runReportRequestBuilder.setKeepEmptyRows(optionalSettings.isKeepEmptyRows()) : runReportRequestBuilder;
		runReportRequestBuilder = optionalSettings.isReturnPropertyQuota() ?  runReportRequestBuilder.setReturnPropertyQuota(optionalSettings.isReturnPropertyQuota()): runReportRequestBuilder;
		
		if(cohortSpec != null) {
			CohortSpec gaCohortSpec = buildCohortRequest(cohortSpec);

			runReportRequestBuilder.setCohortSpec(gaCohortSpec);
		}
		
		return runReportRequestBuilder;
	}
	
	private FilterExpression buildExpressionFilterRequest(FilterExpressionParameter expressionFilter) {
		FilterExpression gaFilterExpression = null;
		if(expressionFilter.getFilter() != null) {
			gaFilterExpression = buildSpecificFilter(expressionFilter.getFilter());
		} else if (!expressionFilter.getOrGroupFilters().isEmpty()){
			List<FilterExpression> filters = new ArrayList<>();
			for (FilterParameter filter : expressionFilter.getOrGroupFilters() ) {
				filters.add(buildSpecificFilter(filter));
			}
			gaFilterExpression = FilterExpression.newBuilder().setOrGroup(FilterExpressionList.newBuilder().addAllExpressions(filters)).build();
			
		} else if (!expressionFilter.getAndGroupFilters().isEmpty()){
			List<FilterExpression> filters = new ArrayList<>();
			for (FilterParameter filter : expressionFilter.getAndGroupFilters() ) {
				filters.add(buildSpecificFilter(filter));
			}
			gaFilterExpression = FilterExpression.newBuilder().setAndGroup(FilterExpressionList.newBuilder().addAllExpressions(filters)).build();
			
		}
		return gaFilterExpression;
	}
	
	private CohortSpec buildCohortRequest (CohortSpecParameter cohortSpec) {
		List<Cohort> gaCohorts = new ArrayList<>();
		CohortSpec gaCohortSpec = null;
		CohortRangeParameter rangeParameter = cohortSpec.getCohortRange();
		
		gaCohortSpec = CohortSpec.newBuilder()
			.setCohortsRange(CohortsRange.newBuilder()
				.setGranularity(Granularity.valueOf(rangeParameter.getGranularity().getValue()))
				.setStartOffset(rangeParameter.getStartOffset())
				.setEndOffset(rangeParameter.getEndOffset()))
			.build();
		
		for (CohortParameter cohort: cohortSpec.getCohorts()) {
			gaCohorts.add(Cohort.newBuilder()
				.setDimension(cohort.getDimension().getDimensionValue())
				.setDateRange(buildDateRange(cohort.getCohortDateRange()))
				.build());
		}
		
		return gaCohortSpec.toBuilder().addAllCohorts(gaCohorts).build();
	}
	
	private DateRange buildDateRange(DateRangeParameter dateRange) {
		DateRange gaDateRange = DateRange.newBuilder()
				.setStartDate(dateRange.getStartDate())
				.setEndDate(dateRange.getEndDate())
				.buildPartial();
		
		if (dateRange.getDateRangeName() != null) {
			gaDateRange = DateRange.newBuilder(gaDateRange)
					.setName(dateRange.getDateRangeName())
					.build();
		}
		return gaDateRange;
	}
	
	private FilterExpression buildSpecificFilter(FilterParameter filter) {
		SpecificFilterParameter specificFilter = filter.getSpecificFilter();
		FilterExpression gaFilterExpression = null;
		Filter gaFilter = null;
		
		if (specificFilter.getStringFilter() != null) {
			StringFilterParameter stringFilter = specificFilter.getStringFilter();
			StringFilter gaStringFilter =  StringFilter.newBuilder().setValue(specificFilter.getStringFilter().getValue()).build();
			
			if (stringFilter.getMatchType() != null) {
				gaStringFilter = gaStringFilter.toBuilder().setMatchType(MatchType.valueOf(stringFilter.getMatchType().getMatchTypeValue())).build();
			}
			
			gaFilter = Filter.newBuilder().setStringFilter(gaStringFilter).build();
		} else if(specificFilter.getInListFilter() != null) {
			gaFilter = Filter.newBuilder().setInListFilter(InListFilter.newBuilder().addAllValues(specificFilter.getInListFilter().getValues())).build();
		} else if (specificFilter.getNumericFilter() != null) {
			NumericFilterParameter numericFilter = specificFilter.getNumericFilter();
			NumericFilter gaNumericFilter = NumericFilter.newBuilder().setValue(NumericValue.newBuilder().setDoubleValue(numericFilter.getValue())).build();
			
			if(numericFilter.getOperation() != null) {
				gaNumericFilter = gaNumericFilter.toBuilder().setOperation(Operation.valueOf(numericFilter.getOperation().getOperationValue())).build();
			}
			
			gaFilter = Filter.newBuilder().setNumericFilter(gaNumericFilter).build();
		} else {
			BetweenFilterParameter betweenFilter = specificFilter.getBetweenFilter();
			
			gaFilter = Filter.newBuilder().setBetweenFilter(BetweenFilter.newBuilder()
					.setFromValue(NumericValue.newBuilder().setDoubleValue(betweenFilter.getFromValue())).setToValue(NumericValue.newBuilder().setDoubleValue(betweenFilter.getToValue()))
				).build();
		}
		
		gaFilter = gaFilter.toBuilder().setFieldName(filter.getFieldName()).build();
		gaFilterExpression = FilterExpression.newBuilder().setFilter(gaFilter).build();
		
		if(filter.getIsNot())
			return FilterExpression.newBuilder().setNotExpression(gaFilterExpression).build();
		return gaFilterExpression;
	}
	
	private Builder buildAdvancedRequest(Builder runReportRequestBuilder, InputStream request) {
		Gson gson = new Gson();
		ReportRequest reportRequest = gson.fromJson(new InputStreamReader(request), ReportRequest.class);
		
		List<DateRange> dateRanges = new ArrayList<>();
		if(reportRequest.getDateRanges() != null) {
			for(DateRangeRequest dateRangeRequest : reportRequest.getDateRanges()) {
				dateRanges.add(buildDateRange(dateRangeRequest));
			}
		}
		
		runReportRequestBuilder.addAllDateRanges(dateRanges);
		
		List<Dimension> gaDimensions = new ArrayList<>();
		if(reportRequest.getDimensions() != null) {
			for (SimpleItemRequest item : reportRequest.getDimensions()) {
				gaDimensions.add(Dimension.newBuilder().setName(item.getName()).build());
			}
		}
		
		runReportRequestBuilder.addAllDimensions(gaDimensions);
		
		List<Metric> gaMetrics = new ArrayList<>();
		if(reportRequest.getMetrics() != null) {
			for (SimpleItemRequest item : reportRequest.getMetrics()) {
				gaMetrics.add(Metric.newBuilder().setName(item.getName()).build());
			}
		}
		
		runReportRequestBuilder.addAllMetrics(gaMetrics);
		
		if(reportRequest.getDimensionFilter() != null) {
			runReportRequestBuilder.setDimensionFilter(buildExpressionFilterRequest(reportRequest.getDimensionFilter()));
		}
		
		if(reportRequest.getMetricFilter() != null) {
			runReportRequestBuilder.setMetricFilter(buildExpressionFilterRequest(reportRequest.getMetricFilter()));
		}
		
		return runReportRequestBuilder;
	}
	
	private DateRange buildDateRange(DateRangeRequest dateRange) {
		DateRange gaDateRange = DateRange.newBuilder()
				.setStartDate(dateRange.getStartDate())
				.setEndDate(dateRange.getEndDate())
				.buildPartial();
		
		if (dateRange.getName() != null) {
			gaDateRange = DateRange.newBuilder()
					.mergeFrom(gaDateRange)
					.setName(dateRange.getName())
					.build();
		}
		return gaDateRange;
	}
	
	private FilterExpression buildExpressionFilterRequest(FilterExpressionRequest expressionFilter) {
		FilterExpression gaFilterExpression = null;
		List<FilterExpression> filters = new ArrayList<>();
		if(expressionFilter.getFilter() != null) {
			gaFilterExpression = buildSpecificFilter(expressionFilter.getFilter());
		} else if (expressionFilter.getOrGroup() != null && !expressionFilter.getOrGroup().isEmpty()){
			for (FilterExpressionRequest filter : expressionFilter.getOrGroup() ) {
				filters.add(buildExpressionFilterRequest(filter));
			}
			
			gaFilterExpression = FilterExpression.newBuilder().setOrGroup(FilterExpressionList.newBuilder().addAllExpressions(filters)).build();
		} else if (expressionFilter.getAndGroup() != null && !expressionFilter.getAndGroup().isEmpty()){
			for (FilterExpressionRequest filter : expressionFilter.getAndGroup() ) {
				filters.add(buildExpressionFilterRequest(filter));
			}
			
			gaFilterExpression = FilterExpression.newBuilder().setAndGroup(FilterExpressionList.newBuilder().addAllExpressions(filters)).build();
		} else if (expressionFilter.getNotExpression() != null) {
			gaFilterExpression = FilterExpression.newBuilder().setNotExpression(buildExpressionFilterRequest(expressionFilter.getNotExpression())).build();
		} else {
			gaFilterExpression = FilterExpression.getDefaultInstance();
		}
		
		return gaFilterExpression;
	}
	
	private FilterExpression buildSpecificFilter(FilterRequest filter) {
		FilterExpression gaFilterExpression = null;

		String name = filter.getFieldName();
		if (name == null) {
			name = "";
		}
		Filter gaFilter = Filter.newBuilder().setFieldName(name).build();
		
		if (filter.getStringFilter() != null) {
			StringFilterRequest stringFilter = filter.getStringFilter();
			StringFilter gaStringFilter =  StringFilter.newBuilder().setValue(stringFilter.getValue()).build();
			
			if (stringFilter.getMatchType() != null) {
				gaStringFilter = gaStringFilter.toBuilder().setMatchType(MatchType.valueOf(stringFilter.getMatchType())).build();
			}
			
			gaFilter = Filter.newBuilder(gaFilter).setStringFilter(gaStringFilter).build();
		} else if(filter.getInListFilter() != null) {
			gaFilter = Filter.newBuilder(gaFilter).setInListFilter(InListFilter.newBuilder().addAllValues(filter.getInListFilter())).build();
		} else if (filter.getNumericFilter() != null) {
			NumericFilterRequest numericFilter = filter.getNumericFilter();
			NumericFilter gaNumericFilter = NumericFilter.newBuilder().setValue(NumericValue.newBuilder().setDoubleValue(numericFilter.getValue())).build();
			
			if(numericFilter.getOperation() != null) {
				gaNumericFilter = gaNumericFilter.toBuilder().setOperation(Operation.valueOf(numericFilter.getOperation())).build();
			}
			
			gaFilter = Filter.newBuilder(gaFilter).setNumericFilter(gaNumericFilter).build();
		} else if(filter.getBetweenFilter() != null) {
			BetweenFilterRequest betweenFilter = filter.getBetweenFilter();
			
			gaFilter = Filter.newBuilder(gaFilter).setBetweenFilter(BetweenFilter.newBuilder()
					.setFromValue(NumericValue.newBuilder().setDoubleValue(betweenFilter.getFromValue())).setToValue(NumericValue.newBuilder().setDoubleValue(betweenFilter.getToValue()))
				).build();
		}
		
		gaFilterExpression = FilterExpression.newBuilder().setFilter(gaFilter).build();
		
		return gaFilterExpression;
	}
	
	private ReportResponse generateResponse(RunReportResponse response) {
		ReportResponse reportResponse = new ReportResponse();
		reportResponse.setCurrencyCode(response.getMetadata().getCurrencyCode());
		reportResponse.setTimeZone(response.getMetadata().getTimeZone());
		
		for(DimensionHeader dimensionHeader : response.getDimensionHeadersList()) {
			SimpleObject value = new SimpleObject();
			value.setName(dimensionHeader.getName());
			reportResponse.getDimensionHeaders().add(value);
			
		}
		
		for(MetricHeader metricHeader : response.getMetricHeadersList()) {
			MetricHeaderResponse value = new MetricHeaderResponse();
			value.setName(metricHeader.getName());
			value.setMetrictType(MetricType.valueOf(metricHeader.getType().toString()));
			reportResponse.getMetricHeaders().add(value);
		}
		
		for(Row row : response.getRowsList()) {
			RowResponse rowResponse = new RowResponse();
			for (DimensionValue dimension : row.getDimensionValuesList()) {
				SimpleValue value = new SimpleValue();
				value.setValue(dimension.getValue());
				rowResponse.getMetricValues().add(value);
			}
			
			for (MetricValue metric : row.getMetricValuesList()) {
				SimpleValue value = new SimpleValue();
				value.setValue(metric.getValue());
				rowResponse.getDimensionValues().add(value);
			}
			
			reportResponse.getRows().add(rowResponse);
		}
		
		if(response.hasPropertyQuota()) {
			PropertyQuota propertyQuota = response.getPropertyQuota();
			PropertyQuotaResponse propertyQuotaResponse = reportResponse.getPropertyQuota();
			
			propertyQuotaResponse.getConcurrentRequests().setConsumed(propertyQuota.getConcurrentRequests().getConsumed());
			propertyQuotaResponse.getConcurrentRequests().setRemaining(propertyQuota.getConcurrentRequests().getRemaining());
			propertyQuotaResponse.getPotentiallyThresholdedRequestsPerHour().setConsumed(propertyQuota.getPotentiallyThresholdedRequestsPerHour().getConsumed());
			propertyQuotaResponse.getPotentiallyThresholdedRequestsPerHour().setRemaining(propertyQuota.getPotentiallyThresholdedRequestsPerHour().getRemaining());
			propertyQuotaResponse.getServerErrorsPerProjectPerHour().setConsumed(propertyQuota.getServerErrorsPerProjectPerHour().getConsumed());
			propertyQuotaResponse.getServerErrorsPerProjectPerHour().setRemaining(propertyQuota.getServerErrorsPerProjectPerHour().getRemaining());
			propertyQuotaResponse.getTokensPerDay().setConsumed(propertyQuota.getTokensPerDay().getConsumed());
			propertyQuotaResponse.getTokensPerDay().setRemaining(propertyQuota.getTokensPerDay().getRemaining());
			propertyQuotaResponse.getTokensPerHour().setConsumed(propertyQuota.getTokensPerHour().getConsumed());
			propertyQuotaResponse.getTokensPerHour().setRemaining(propertyQuota.getTokensPerHour().getRemaining());
			propertyQuotaResponse.getTokensPerProjectPerHour().setConsumed(propertyQuota.getTokensPerProjectPerHour().getConsumed());
			propertyQuotaResponse.getTokensPerProjectPerHour().setRemaining(propertyQuota.getTokensPerProjectPerHour().getRemaining());
		} else {
			reportResponse.setPropertyQuota(null);
		}
		
		return reportResponse;
	}

}
