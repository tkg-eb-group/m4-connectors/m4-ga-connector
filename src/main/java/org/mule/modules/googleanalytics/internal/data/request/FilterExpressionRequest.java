/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.data.request;

import java.util.List;

public class FilterExpressionRequest {
	private FilterRequest filter;
	private List<FilterExpressionRequest> andGroup;
	private List<FilterExpressionRequest> orGroup;
	private FilterExpressionRequest notExpression;
	
	
	public FilterRequest getFilter() {
		return filter;
	}
	
	public List<FilterExpressionRequest> getAndGroup() {
		return andGroup;
	}
		
	public List<FilterExpressionRequest> getOrGroup() {
		return orGroup;
	}
		
	public FilterExpressionRequest getNotExpression() {
		return notExpression;
	}
	
}
