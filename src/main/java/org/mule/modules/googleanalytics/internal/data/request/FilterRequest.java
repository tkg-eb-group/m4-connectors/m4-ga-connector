/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.data.request;

import java.util.List;

public class FilterRequest {
	private String fieldName;
	private StringFilterRequest stringFilter;
	private List<String> inListFilter;
	private NumericFilterRequest numericFilter;
	private BetweenFilterRequest betweenFilter;
	
	
	public String getFieldName() {
		return fieldName;
	}
	
	public StringFilterRequest getStringFilter() {
		return stringFilter;
	}
	
	public List<String> getInListFilter() {
		return inListFilter;
	}
		
	public NumericFilterRequest getNumericFilter() {
		return numericFilter;
	}
	
	public BetweenFilterRequest getBetweenFilter() {
		return betweenFilter;
	}

}
