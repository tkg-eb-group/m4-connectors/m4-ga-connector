/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.data.response;

public class PropertyQuotaResponse {
	private PropertyQuotaRemainingResponse tokensPerDay;
	private PropertyQuotaRemainingResponse tokensPerHour;
	private PropertyQuotaRemainingResponse concurrentRequests;
	private PropertyQuotaRemainingResponse serverErrorsPerProjectPerHour;
	private PropertyQuotaRemainingResponse potentiallyThresholdedRequestsPerHour;
	private PropertyQuotaRemainingResponse tokensPerProjectPerHour;
	
	public PropertyQuotaResponse() {
		tokensPerDay = new PropertyQuotaRemainingResponse();
		tokensPerHour = new PropertyQuotaRemainingResponse();
		concurrentRequests = new PropertyQuotaRemainingResponse();
		serverErrorsPerProjectPerHour = new PropertyQuotaRemainingResponse();
		potentiallyThresholdedRequestsPerHour = new PropertyQuotaRemainingResponse();
		tokensPerProjectPerHour = new PropertyQuotaRemainingResponse();
	}
	
	
	public PropertyQuotaRemainingResponse getTokensPerDay() {
		return tokensPerDay;
	}

	public PropertyQuotaRemainingResponse getTokensPerHour() {
		return tokensPerHour;
	}

	public PropertyQuotaRemainingResponse getConcurrentRequests() {
		return concurrentRequests;
	}

	public PropertyQuotaRemainingResponse getServerErrorsPerProjectPerHour() {
		return serverErrorsPerProjectPerHour;
	}

	public PropertyQuotaRemainingResponse getPotentiallyThresholdedRequestsPerHour() {
		return potentiallyThresholdedRequestsPerHour;
	}

	public PropertyQuotaRemainingResponse getTokensPerProjectPerHour() {
		return tokensPerProjectPerHour;
	}

}
