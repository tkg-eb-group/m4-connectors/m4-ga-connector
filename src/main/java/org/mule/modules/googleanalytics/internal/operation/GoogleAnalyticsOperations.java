/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.operation;

import static org.mule.runtime.api.meta.ExpressionSupport.NOT_SUPPORTED;
import static org.mule.runtime.api.meta.ExpressionSupport.SUPPORTED;
import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.mule.modules.googleanalytics.api.params.CohortSpecParameter;
import org.mule.modules.googleanalytics.api.params.OptionalSettingsParameter;
import org.mule.modules.googleanalytics.api.params.ReportRequestParameter;
import org.mule.modules.googleanalytics.internal.connection.GoogleAnalyticsConnection;
import org.mule.modules.googleanalytics.internal.error.exception.GoogleAnalyticsException;
import org.mule.modules.googleanalytics.internal.error.provider.ExecutionErrorTypeProvider;
import org.mule.modules.googleanalytics.internal.util.GoogleAnalyticsUtility;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

/**
 * This class is a container for operations, every public method in this class will be taken as an extension operation.
*/
public class GoogleAnalyticsOperations {

	/**
	 * 
	 * Use this operation for retrieving a report of Google Analytics
	 *
	 * @param analyticsConnection A valid configured connection.
	 * @param propertyId An Unique Google Analytics 4 Property ID to get Analytics Data.
	 * @param reportRequest Section to define the dimensions, metrics and filtering for the request, an advanced version can be used sending a valid string of a JSON request.
	 * @param optionalSettings Use this section to define more parameters like pagination, currency code of enable the quota info.
	 * @param cohortSpec Use this section in the Advanced tab to configure a cohort specification for this report.
	 * @return A JSON google analytics report with the results based on the configured query
	 * @throws GoogleAnalyticsException
	 */

	@MediaType(value = "application/json")
	@OutputJsonType(schema = "schema/generate-report-schema.json")
	@Throws(ExecutionErrorTypeProvider.class)
	@Summary("Operation that allows obtaining Google Analytics data")
	public InputStream runReport(@Connection GoogleAnalyticsConnection analyticsConnection,
			@Expression(SUPPORTED) @Summary("An Unique Google Analytics Profile ID to get Analytics Data") String propertyId,
			@Expression(NOT_SUPPORTED) @ParameterGroup(name = "Report request") ReportRequestParameter reportRequest,
			@Expression(SUPPORTED) @ParameterGroup(name = "Optional Settings") OptionalSettingsParameter optionalSettings,
			@Expression(NOT_SUPPORTED) @ParameterDsl(allowReferences = false) @Optional @NullSafe @DisplayName("Cohort") @Placement(tab = ADVANCED_TAB) @Summary("Cohort spec summ") CohortSpecParameter cohortSpec) {
		GoogleAnalyticsUtility analyticsUtility = new GoogleAnalyticsUtility();
		return new ByteArrayInputStream(analyticsUtility.runReport(analyticsConnection, propertyId, reportRequest, optionalSettings, cohortSpec).getBytes(StandardCharsets.UTF_8));
	}
}
