/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.data.response;

public enum MetricType {
	METRIC_TYPE_UNSPECIFIED,
	TYPE_INTEGER,
	TYPE_FLOAT,
	TYPE_SECONDS,
	TYPE_MILLISECONDS,
	TYPE_MINUTES,
	TYPE_HOURS,
	TYPE_STANDARD,
	TYPE_CURRENCY,
	TYPE_FEET,
	TYPE_MILES,
	TYPE_METERS,
	TYPE_KILOMETERS
}
