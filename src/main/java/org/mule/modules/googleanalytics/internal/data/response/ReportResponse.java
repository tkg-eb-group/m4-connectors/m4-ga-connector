/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.internal.data.response;

import java.util.List;
import java.util.ArrayList;

public class ReportResponse {
	private List<SimpleObject> dimensionHeaders;
	private List<MetricHeaderResponse> metricHeaders;
	private List<RowResponse> rows;
	private long rowCount;
	private String currencyCode;
	private String timeZone;
	private PropertyQuotaResponse propertyQuota;
	
	public ReportResponse() {
		dimensionHeaders = new ArrayList<>();
		metricHeaders = new ArrayList<>();
		rows = new ArrayList<>();
		propertyQuota = new PropertyQuotaResponse();
	}
	
	
	public List<SimpleObject> getDimensionHeaders() {
		return dimensionHeaders;
	}

	public List<MetricHeaderResponse> getMetricHeaders() {
		return metricHeaders;
	}

	public List<RowResponse> getRows() {
		return rows;
	}
	
	public long getRowCount() {
		return rowCount;
	}
	
	public void setRowCount(long rowCount) {
		this.rowCount = rowCount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public PropertyQuotaResponse getPropertyQuota() {
		return propertyQuota;
	}

	public void setPropertyQuota(PropertyQuotaResponse propertyQuota) {
		this.propertyQuota = propertyQuota;
	}
	
}
