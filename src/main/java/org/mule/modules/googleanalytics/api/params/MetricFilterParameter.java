/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import static org.mule.runtime.api.meta.ExpressionSupport.NOT_SUPPORTED;

import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;

public class MetricFilterParameter {
	public MetricFilterParameter() {
		// Default constructor
	}
	
	@ParameterGroup(name = "Metric Filter")
	@Expression(NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false) 
	FilterExpressionParameter metricFilter;

	
	public FilterExpressionParameter getMetricFilter() {
		return metricFilter;
	}
	
	
}
