/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

public class DateRangeParameter {
	public DateRangeParameter() {
		// Default Constructor
	}
	
	@Parameter
	@Optional
	private String dateRangeName;
	
	@Parameter
	private String startDate;
	
	@Parameter
	private String endDate;
	
	
	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getDateRangeName() {
		return dateRangeName;
	}
}
