/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import static org.mule.runtime.api.meta.ExpressionSupport.NOT_SUPPORTED;

import java.util.List;

import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class RunReportRequestParameter {
	@Parameter
	@Expression(NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Optional
	@NullSafe
	@Summary("A set of valid date ranges, this parameter is required and should have at least one element. When using cohort request, this shouldn't be included")
	List<DateRangeParameter> dateRanges;
	
	@Parameter
	@Expression(NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Optional
	@NullSafe
	@Summary("A set of valid dimensions, this can be optional, but at least one of this and/or metrics are required, both are recommended")
	List<DimensionParameter> dimensions;
	
	@Parameter
	@Expression(NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Optional
	@NullSafe
	@Summary("A set of valid metrics, this can be optional, but at least one of this and/or metrics are required, both are recommended")
	List<MetricParameter> metrics;
	
	@Parameter
	@Expression(NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Optional
	@Summary("Dimension filters let you ask for only specific dimension values in the report.")
	DimensionFilterParameter dimensionFilter;
	
	@Parameter
	@Expression(NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Optional
	@Summary("The filter clause of metrics. Applied after aggregating the report's rows, similar to SQL having-clause. Dimensions cannot be used in this filter.")
	MetricFilterParameter metricFilter;
	

	public List<DateRangeParameter> getDateRanges() {
		return dateRanges;
	}

	public List<DimensionParameter> getDimensions() {
		return dimensions;
	}

	public List<MetricParameter> getMetrics() {
		return metrics;
	}

	public DimensionFilterParameter getDimensionFilter() {
		return dimensionFilter;
	}

	public MetricFilterParameter getMetricFilter() {
		return metricFilter;
	}

}
