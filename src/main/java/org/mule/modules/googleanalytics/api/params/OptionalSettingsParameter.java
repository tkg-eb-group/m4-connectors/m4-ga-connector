/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import static org.mule.runtime.api.meta.ExpressionSupport.SUPPORTED;

import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class OptionalSettingsParameter {
	@Parameter
	@Expression(SUPPORTED)
	@Optional
	@Summary("The number of rows returned by the report")
	int limit;

	@Parameter
	@Expression(SUPPORTED)
	@Optional
	@Summary("Use this value to paginate")
	int offset;

	@Parameter
	@Expression(SUPPORTED)
	@Optional
	@Summary("A currency code in ISO4217 format, such as \"AED\", \"USD\", \"JPY\". If the field is empty, the report uses the property's default currency")
	String currencyCode;

	@Parameter
	@Expression(SUPPORTED)
	@Optional
	@Summary("If false or unspecified, each row with all metrics equal to 0 will not be returned")
	boolean keepEmptyRows;

	@Parameter
	@Expression(SUPPORTED)
	@Optional
	@Summary("Toggles whether to return the current state of this Analytics Property's quota")
	boolean returnPropertyQuota;

	public int getLimit() {
		return limit;
	}

	public int getOffset() {
		return offset;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public boolean isKeepEmptyRows() {
		return keepEmptyRows;
	}

	public boolean isReturnPropertyQuota() {
		return returnPropertyQuota;
	}

}
