/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import java.util.List;

import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class InListFilterParameter {
	
	public InListFilterParameter() {
		// Default Constructor
	}
	
	@Parameter
	@Summary("The list of string values. Must be non-empty.")
	private List<String> values;
	
	@Parameter
	@Optional
	@Summary("If true, the string value is case sensitive.")
	private boolean caseSensitive;

	
	public List<String> getValues() {
		return values;
	}

	public boolean isCaseSensitive() {
		return caseSensitive;
	}

}
