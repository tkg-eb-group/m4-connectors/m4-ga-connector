/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import org.mule.modules.googleanalytics.api.domain.Operation;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class NumericFilterParameter {
	
	public NumericFilterParameter() {
		// Default Constructor
	}
	@Parameter
	@Optional
	@Summary("The operation type for this filter.")
	private Operation operation;
	
	@Parameter
	@Summary("A numeric value.")
	private long value;
	
	
	public Operation getOperation() {
		return operation;
	}
	public long getValue() {
		return value;
	}

}
