/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.domain;

public enum Operation {
	EQUAL("EQUAL"),	
	LESS_THAN("LESS_THAN"), 	
	LESS_THAN_OR_EQUAL("LESS_THAN_OR_EQUAL"), 	
	GREATER_THAN("GREATER_THAN"),
	GREATER_THAN_OR_EQUAL("GREATER_THAN_OR_EQUAL");
	
	Operation(String operation){
		this.operationValue = operation;
	}
	
	private final String operationValue;

	public String getOperationValue() {
		return operationValue;
	}

}
