/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import java.util.List;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.ExclusiveOptionals;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@ExclusiveOptionals(isOneRequired = true)
public class FilterExpressionParameter {
	public FilterExpressionParameter() {
		// Default constructor
	}
	
	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Optional
	@Summary("An expression to filter dimension or metric values.")
	private FilterParameter filter;
	
	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Optional
	@NullSafe
	@DisplayName("orGroup")
	@Summary("The FilterExpressions in orGroup have an OR relationship.")
	private List<FilterParameter> orGroupFilters;
	
	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Optional
	@NullSafe
	@DisplayName("andGroup")
	@Summary("The FilterExpressions in andGroup have an AND relationship.")
	private List<FilterParameter> andGroupFilters;
	

	public List<FilterParameter> getOrGroupFilters() {
		return orGroupFilters;
	}

	public FilterParameter getFilter() {
		return filter;
	}
	
	public List<FilterParameter> getAndGroupFilters() {
		return andGroupFilters;
	}

}
