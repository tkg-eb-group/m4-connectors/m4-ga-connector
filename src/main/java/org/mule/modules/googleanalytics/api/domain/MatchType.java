/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.domain;

public enum MatchType {
	EXACT("EXACT"), 
	BEGINS_WITH("BEGINS_WITH"), 
	ENDS_WITH("ENDS_WITH"), 
	CONTAINS("CONTAINS"), 
	FULL_REGEXP("FULL_REGEXP"), 
	PARTIAL_REGEXP("PARTIAL_REGEXP");
	
	MatchType(String matchType){
		matchTypeValue = matchType;
	}
	
	private final String matchTypeValue;
	
	public String getMatchTypeValue() {
		return matchTypeValue;
	}
}
