/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class FilterParameter {
	
	public FilterParameter() {
		// Default constructor
	}
	
	@Parameter
	@Summary("It should be a valid dimension/metric name")
	private String fieldName;
	
	@Parameter
	@Optional
	@Summary("The filter can be negate using this parameter")
	private boolean isNot;
	
	@ParameterGroup(name = "Specific Filter")
	private SpecificFilterParameter specificFilter;
	
	
	public String getFieldName() {
		return fieldName;
	}

	public boolean getIsNot() {
		return isNot;
	}

	public SpecificFilterParameter getSpecificFilter() {
		return specificFilter;
	}

}
