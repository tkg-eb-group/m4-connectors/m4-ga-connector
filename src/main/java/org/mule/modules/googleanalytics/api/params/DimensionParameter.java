/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class DimensionParameter {
	public DimensionParameter() {
		// Default Constructor
	}
	
	@Parameter
	@DisplayName("Dimension Name")
	@Summary("A valid dimension name in the Analytics Data API")
	private String dimensionValue;
	
	
	public String getDimensionValue() {
		return dimensionValue;
	}
}
