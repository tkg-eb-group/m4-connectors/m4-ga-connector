/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import org.mule.modules.googleanalytics.api.domain.MatchType;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class StringFilterParameter {
	
	public StringFilterParameter() {
		// Default Constructor
	}
	
	@Parameter
	@Optional
	@Summary("The match type for this filter.")
	private MatchType matchType;
	
	@Parameter
	@Summary("The string value used for the matching.")
	private String value;
	
	@Parameter
	@Optional
	@Summary("If true, the string value is case sensitive.")
	private boolean caseSensitive;
	
	
	public MatchType getMatchType() {
		return matchType;
	}
	
	public String getValue() {
		return value;
	}
	
	public boolean isCaseSensitive() {
		return caseSensitive;
	}
	
}
