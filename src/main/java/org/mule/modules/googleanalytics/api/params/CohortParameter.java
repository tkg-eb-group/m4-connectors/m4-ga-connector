/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import static org.mule.runtime.api.meta.ExpressionSupport.NOT_SUPPORTED;
import static org.mule.runtime.api.meta.ExpressionSupport.SUPPORTED;

import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class CohortParameter {
	
	public CohortParameter() {
		// Default Constructor
	}
	
	@Parameter
	@Expression(SUPPORTED)
	@Optional
	@Summary("Assigns a name to this cohort. The dimension cohort is valued to this name in a report response.")
	private String name;
	
	@Parameter
	@Expression(NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Summary("Dimension used by the cohort. Required and only supports firstSessionDate.")
	private DimensionParameter dimension;
	
	@Parameter
	@Expression(NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Summary("The cohort selects users whose first touch date is between start date and end date defined in the dateRange.")
	private DateRangeParameter cohortDateRange;
	
	
	public String getName() {
		return name;
	}
	public DimensionParameter getDimension() {
		return dimension;
	}
	public DateRangeParameter getCohortDateRange() {
		return cohortDateRange;
	}

}
