/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import static org.mule.runtime.api.meta.ExpressionSupport.NOT_SUPPORTED;
import static org.mule.runtime.api.meta.ExpressionSupport.SUPPORTED;

import org.mule.modules.googleanalytics.api.domain.Granularity;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class CohortRangeParameter {
	public CohortRangeParameter() {
		// Default Constructor
	}
	
	@Parameter
	@Expression(NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Summary("The granularity used to interpret the startOffset and endOffset for the extended reporting date range for a cohort report.")
	private Granularity granularity;
	
	@Parameter
	@Expression(SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Optional
	@Summary("Specifies the start date of the extended reporting date range for a cohort report.")
	private int startOffset;
	
	@Parameter
	@Expression(SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Summary("Specifies the end date of the extended reporting date range for a cohort report.")
	private int endOffset;
	
	
	public Granularity getGranularity() {
		return granularity;
	}
	
	public int getStartOffset() {
		return startOffset;
	}
	
	public int getEndOffset() {
		return endOffset;
	}

}
