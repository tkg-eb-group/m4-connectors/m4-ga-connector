/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class BetweenFilterParameter {
	
	public BetweenFilterParameter(){
		// Default Constructor
	}
	
	@Parameter
	@Summary("Begins with this number.")
	private long fromValue;
	
	@Parameter
	@Summary("Ends with this number.")
	private long toValue;
	
	
	public long getFromValue() {
		return fromValue;
	}
	public long getToValue() {
		return toValue;
	}
	
}
