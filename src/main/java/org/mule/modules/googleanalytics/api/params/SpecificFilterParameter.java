/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.ExclusiveOptionals;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@ExclusiveOptionals(isOneRequired = true)
public class SpecificFilterParameter {
	
	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@Optional
	@Summary("The filter for string.")
	private StringFilterParameter stringFilter;
	
	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@Optional
	@Summary("The result needs to be in a list of string values.")
	private InListFilterParameter inListFilter;
	
	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@Optional
	@Summary("Filters for numeric value.")
	private NumericFilterParameter numericFilter;
	
	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@Optional
	@Summary("To express that the result needs to be between two numbers.")
	private BetweenFilterParameter betweenFilter;
	
	
	public StringFilterParameter getStringFilter() {
		return stringFilter;
	}

	public InListFilterParameter getInListFilter() {
		return inListFilter;
	}
	
	public NumericFilterParameter getNumericFilter() {
		return numericFilter;
	}

	public BetweenFilterParameter getBetweenFilter() {
		return betweenFilter;
	}

}
