/**
 * (c) 2003-2023 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.googleanalytics.api.params;

import java.io.InputStream;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Content;
import org.mule.runtime.extension.api.annotation.param.ExclusiveOptionals;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@ExclusiveOptionals(isOneRequired = true)
public class ReportRequestParameter {
	@Parameter
	@Optional
	@Content
	@Summary("Parameter to perform advanced filtering request, also you can define dimensions, metrics and date ranges")
	private InputStream advanced;
	
	@Parameter
	@Optional
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@ParameterDsl(allowReferences = false)
	@Summary("Parameter to construct your request using the UI, admits a basic filtering")
	private RunReportRequestParameter basic;
	

	public InputStream getAdvanced() {
		return advanced;
	}

	public RunReportRequestParameter getBasic() {
		return basic;
	}

}
